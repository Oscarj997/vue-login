import { createStore } from "vuex";

export default createStore({
  state: {
    user: null,
  },
  getters: {},
  mutations: {
    SET_USER(state, user) {//Se ejecuta cuando el usuuario se logea
      state.currentUser = user;//guardamos los datos en el state
      window.localStorage.currentUser = JSON.stringify(user);//gyardamos la session en la localstoge 
    },
    SET_LOGGED_USER(state, user) {
      //Verifica si el usuario esta logeado
      state.currentUser = user;
      state.islogged = true;
    },
    LOGOUT(state) {//se ejecuta para cerrar session
      state.currentUser = {};
      state.islogged = false;//cambiamos la variable para saber que esta desconectado
      window.localStorage.clear();//limpiamos el usuario de la localstorage
    },
  },
  actions: {
    login({ commit }, data) {
      console.log(data);
      commit("SET_USER", data.data);
    },
    SetUserInfo({ commit }, User) {
      if (User != null) {
        commit("SET_LOGGED_USER", User);
      } else {
        console.log("no hay usuario");
      }
    },
    Logout({ commit }) {
      commit("LOGOUT");
      console.log("Cerraste la sesion");
    },
  },
  modules: {},
});
