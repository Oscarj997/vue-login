import { createApp } from 'vue'
import {createRouter,createWebHashHistory} from 'vue-router'
import App from './App.vue'
import home from '../src/components/HomeComponet.vue'
import login from '../src/components/LoginComponet.vue'
import store from "./store";
const routes = [
    {
      path: '/',
      name: 'login',
      component: login
    },{
        path:'/home',
        name: "home",
        component:home

    }

]

    const router =createRouter({
        history:createWebHashHistory(),
        routes
    })
const app = createApp(App)
app.use(router)
app.use(store)

.mount('#app')