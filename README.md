# prueba

# Vue Login

## Descripción

Crear una forma de "login" en VueJS, la forma debe tener un campo de usuario y de contraseña. Debe conectarse al [AUTH API](https://gitlab.com/leackstat/auth_api) creado en Laravel para validar al usuario.


## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
